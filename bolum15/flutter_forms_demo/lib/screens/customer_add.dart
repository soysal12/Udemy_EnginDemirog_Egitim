import 'package:flutter/material.dart';
import 'package:flutter_forms_demo/mixins/validation_mixin.dart';
import 'package:flutter_forms_demo/models/customer.dart';

class CustomerAdd extends StatefulWidget {
  const CustomerAdd({super.key});

  @override
  State<CustomerAdd> createState() => _CustomerAddState();
}

class _CustomerAddState extends State<CustomerAdd> with ValidationMixin {
  final formKey = GlobalKey<FormState>();
  final customer = Customer();
  bool passwordCheck = true;
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(20),
        child: Form(
            key: formKey,
            child: Column(
              children: [
                firstNameField(),
                lastNameField(),
                emailtNameField(),
                passwordNameField(),
                SizedBox(
                  height: 20,
                ),
                submitButton(),
              ],
            )));
  }

  Widget firstNameField() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration:
          InputDecoration(labelText: "Adınız", hintText: "Ör : İbrahim"),
      validator: validateFirstName, // altta hata mesajı vermek için kullanılır
      onSaved: (value) {
        customer.firstName = value;
      },
    );
  }

  Widget lastNameField() {
    return TextFormField(
      validator: validateLasttName,
      keyboardType: TextInputType.name,
      decoration:
          InputDecoration(labelText: "Soyadınız", hintText: "Ör : Soysal"),
      onSaved: (value) {
        customer.lastName = value;
      },
    );
  }

  Widget emailtNameField() {
    return TextFormField(
      validator: validateEmailName,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          labelText: "Email", hintText: "Ör :  isoysal12@gmail.com"),
      onSaved: (value) {
        customer.email = value;
      },
    );
  }

  Widget passwordNameField() {
    return TextFormField(
      obscureText: passwordCheck, // password u gizlemek için kullanılır

      decoration: InputDecoration(
        labelText: "Şifre ",
        hintText: "Ör :sifre",
        suffixIcon: IconButton(
            onPressed: () {
              setState(() {
                passwordCheck = !passwordCheck;
              });
            },
            icon: Icon(Icons.remove_red_eye)),
      ),
    );
  }

  Widget submitButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState?.validate() == true) {
            formKey.currentState
                ?.save(); // formun içinde olanları kaydetmek için kullanırız bunu sqllite ve farklı bir yerer kaydedebiliriz
            saveCustomer(customer);
          }
        },
        child: Text("Kaydet"));
  }

  void saveCustomer(Customer customer) {
    // ignore: avoid_print

    AlertDialog alertDialog = AlertDialog(
      content: Text("Kayıt Başarılı"),
      title: Text("${customer.firstName}  ${customer.lastName}"),
    );

    showDialog(
      context: context,
      builder: (context) => alertDialog,
    );
  }
}
