import 'package:flutter/material.dart';
import 'package:flutter_forms_demo/screens/customer_add.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Form Workspace "),
        ),
        body: CustomerAdd(),
      ),
    );
  }
}
