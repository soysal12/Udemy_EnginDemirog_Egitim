class ValidationMixin {
  String? validateFirstName(value) {
    if (value.length < 2) {
      return "isim en az iki karakterli olmalıdır ";
    }
    return null;
  }

  String? validateLasttName(value) {
    if (value.length < 2) {
      return "Soyisim en az iki karakterli olmalıdır ";
    }
    return null;
  }

  String? validateEmailName(value) {
    if (!value.contains("@")) {
      // emailin için @ harfi var mı yok mu diye teyit ettiriyorum
      return "Email Geçersizdir Lütfen Tekrar Deneyiniz !";
    }
    return null;
  }
}
