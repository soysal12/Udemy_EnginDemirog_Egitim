import 'package:flutter/material.dart';

void main() {
  runApp(MyAppDesign());
}

class MyAppDesign extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My First App Demo",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Appbar Title", textDirection: TextDirection.ltr),
        ),
        body: MainPage(),
      ),
    );
  }
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.amber,
      child: Center(
        child: Text(
          "Merhaba \nJr. Mobil Developer\nMr.Soysal",
          textDirection: TextDirection.ltr,
          style: TextStyle(color: Colors.black, fontSize: 50.0),
        ),
      ),
    );
  }
}
