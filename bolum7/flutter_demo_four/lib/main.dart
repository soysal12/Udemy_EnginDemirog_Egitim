import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
      appBar: AppBar(
        title: Text("Appbar Title", textDirection: TextDirection.ltr),
      ),
      body: Material(
        color: Colors.amber,
        child: Center(
          child: Text(
            "Merhaba \nJr. Mobil Developer\nMr.Soysal",
            textDirection: TextDirection.ltr,
            style: TextStyle(color: Colors.black, fontSize: 50.0),
          ),
        ),
      ),
    ),
  ));
}
