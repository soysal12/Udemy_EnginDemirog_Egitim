import 'package:flutter/material.dart';

class Product {
  int? id;
  String? name;
  String? description;
  double? price;

  Product({this.name, this.description, this.price});
  Product.withId({this.id, this.name, this.description, this.price});

  int? get getId => id;
  String? get getName => name;
  String? get getDescription => description;
  double? get getPrice => price;

  set nameSet(String newName) {
    if (newName.length >= 2) {
      name = newName;
    }
  }

  set setDescription(String newDescription) {
    if (newDescription.length >= 2) {
      description = newDescription;
    }
  }

  set setPrice(double newPrice) {
    if (newPrice > 0) {
      price = newPrice;
    }
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map["name"] = name;
    map["description"] = description;
    map["price"] = price;
    if (id != null) {
      map["id"] = id;
    }
    return map;
  }

  Product.fromObject(dynamic o) {
    id = o["id"];
    name = o["name"];
    description = o["description"];
    price = o["price"];
  }
}
