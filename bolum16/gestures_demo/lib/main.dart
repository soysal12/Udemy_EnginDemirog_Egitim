import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  int tabsCount = 0;
  int doubleTabsCount = 0;
  int longPressCount = 0;

  double xPosition = 0.0;
  double yPosition = 0.0;
  double boxSize = 0;
  double fullBoxSize = 150;

  late AnimationController animationController;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        // animasyonun zamanını buradan ayarlıyoruz
        vsync: this,
        duration: const Duration(seconds: 10));
// animasyonun türünü buradan ayarlıyoruz
    animation =
        CurvedAnimation(parent: animationController, curve: Curves.bounceInOut);
    animation.addListener(() {
      setState(() {
        boxSize = fullBoxSize * animation.value;
        // burada box sizeın değerini animasyonun value değeri ile eşitleyip 0 dan 150 ye
        //yani bizim tanımladığımız değere kadar yavaş yavaş büyümesini
        //ekranda anlık görebilmek için yapıyoruz
      });
      centerBox(context); // kutuyu çağırdım
    });

    animationController.forward(); // animasyonun Çalışmasını buradan  sağlıyor
    //  animationController.dispose();
  }

  @override
  void dispose() {
    // disposse bellekten atmak için kullanılır yani ihtiyaç kalmadığını gösterir
    super.dispose();
    // gererksiz kullanımı kaldırmak için yapıyoruz
    animationController.dispose();
    // animasyonla işimiz kalmadığımız için bellekten atıyoruz
  }

  @override
  Widget build(BuildContext context) {
    // if (xPosition == 0.0) {
    //   centerBox(context);
    // }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Gestures Demo"),
      ),
      bottomNavigationBar: Material(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(
              "Tab :  $tabsCount   ,   Double Tab : $doubleTabsCount   ,   Long Press :  $longPressCount "),
        ),
      ),
      body: GestureDetector(
        onTap: () {
          setState(() {
            tabsCount++;
          });
        },
        onDoubleTap: () {
          setState(() {
            doubleTabsCount++;
          });
        },
        onLongPress: () {
          longPressCount++;
        },
        onHorizontalDragUpdate: (DragUpdateDetails value) {
          setState(() {
            double delta = value.delta
                .dx; //delta boyutuyla ilgili kısmı dx x yaatay satırdaki değeri

            xPosition += delta;
          });
        },
        onVerticalDragUpdate: (DragUpdateDetails value) {
          setState(() {
            double delta = value.delta.dy;
            //delta boyutuyla ilgili kısmı dy y dikey  satırdaki değeri

            yPosition += delta;
          });
        },
        child: Stack(
          children: [
            Positioned(
                left: xPosition,
                top: yPosition,
                child: Container(
                  width: boxSize,
                  height: boxSize,
                  color: Colors.deepPurpleAccent,
                ))
          ],
        ),
      ),
    );
  }

  void centerBox(BuildContext context) {
    xPosition = MediaQuery.of(context).size.width / 2 - boxSize / 2;
    // kutunu tama ortadad görünebilmesi için kendi ölçüsününde yarısını çıkartmalıyız

    yPosition = MediaQuery.of(context).size.height / 2 - boxSize / 2 - 70;
    // dikeyde appbarda var olduğu için onunla ilgili bir miktar çıkartma daha yaptık tam orta noktasını bulsun diye

    setState(() {
      xPosition = xPosition;
      yPosition = yPosition;
    });
  }
}
