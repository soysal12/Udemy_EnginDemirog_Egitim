// hocadaki id bende getId dir diğerleride buna benzerdir

class Product {
  int? _id;
  String? _name;
  String? _description;
  double? _price;

  Product(this._name, this._description, this._price);
  Product.withId(this._id, this._name, this._description, this._price);

  int? get getId => _id;
  String? get getName => _name;
  String? get getDescription => _description;
  double? get getPrice => _price;

  set nameSet(String newName) {
    if (newName.length >= 2) {
      _name = newName;
    }
  }

  set setDescription(String newDescription) {
    if (newDescription.length >= 2) {
      _description = newDescription;
    }
  }

  set setPrice(double newPrice) {
    if (newPrice > 0) {
      _price = newPrice;
    }
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    if (_id != null) {
      map["id"] = _id;
    }
    map["name"] = _name;
    map["description"] = _description;
    map["price"] = _price;

    return map;
  }

  Product.fromObject(dynamic o) {
    //gelen verileri kendi tanımladığımız dataya göre yerleştirdik
    _id = o["id"];
    _name = o["name"];
    _description = o["description"];
    _price = double.tryParse(o["price"].toString());
    // sqflite da database de double değer tutamadığımız için
    //price buradadn double ye çevirdik
  }
}
