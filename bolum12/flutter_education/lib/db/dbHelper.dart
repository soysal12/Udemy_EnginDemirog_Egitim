import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_etrade/models/product.dart';

class DbHelper {
  String tblProduct = "Products";
  String colId = "Id";
  String colName = "Name";
  String colDescription = "Description";
  String colPrice = "Price";

  static final DbHelper _dbHelper = DbHelper._internal();

  DbHelper._internal();
  // DbHelper ın STATİC VE constructor sız bir şekilde
  //ÇAĞRILABİLMESİ İÇİN bu kod bloğunu kullandım

  factory DbHelper() {
    // normal bir fonksiyonmuş gibi çağırabilmemizi sağlıyor
    // kullanıcı her çağırdığında aynı dbhelper instance dönmüş olacak
    return _dbHelper;
  }

// Bu kısımda Database ile ilgili işlemler yer almaktadır
  static Database? _db;

  Future<Database?> get db async {
    _db ??= await initializeDb();

    // hoca bu aşağıdaki şekilde yapmış

    /*  Future<Database> get db async {
    if (_db == null) {
      _db = await initializeDb();
    }
    return _db;
  }
*/

    return _db;
  }

// Database de proje için depolama alanı oluşturmanın ilk aşaması

  Future<Database> initializeDb() async {
    // veri tabanı oluşturmak için kullandığımız kod bloğu
    Directory directory = await getApplicationDocumentsDirectory();
    String path = "${directory.path}etrade.db";
    var dbEtrade = await openDatabase(path, version: 1, onCreate: _createDb);

    /*Exception has occurred.
SqfliteDatabaseException (DatabaseException(near "Name": syntax error (code 1 SQLITE_ERROR): , 
while compiling: Create table Products(Id integer primary key Name text ,Description text , Price int) 
sql 'Create table Products(Id integer primary key Name text ,Description text , Price int' args []) */

    /* */
    return dbEtrade;
  }

// oluşturmak istediğimiz veri tabanının iç kısmının nasıl olması gerektiğini gösteren kısım

  void _createDb(Database db, int version) async {
    await db.execute(
        "Create table $tblProduct($colId integer primary key  ,$colName  text , $colDescription  text  , $colPrice int , ) ");
    // colPrice ı int olarak almamızın sebebi Veri tabanında double değeri tutabileceğimiz bir alan olmadığı için
    //int ile tutup dönüşümler ile istediğimiz değere dönüştürüyoruz
  }

  // veri tabanı ile ilgili  insert  işlemlerinin yapıldığı kısım

  Future<int?> insert(Product product) async {
    Database? db = _db;

    var result = await db?.insert(tblProduct, product.toMap());

    return result;
  }

  // Veti tabanının Update Güncelleme Edilme Kısmı burasıdır

  Future<int?> update(Product product) async {
    Database? db = _db;
    var result = await db?.update(tblProduct, product.toMap(),
        // bunun yerine direkt olarka RAWUPDATE  komutunu da kullabiliriz
        where: "$colId =?",
        whereArgs: [product.getId]);

    return result;
  }

  //veri tabanında Delete Silme işlemi burada yapılır
  Future<int?> delete(int id) async {
    Database? db = _db;
    var result = await db?.rawDelete(
        // bunun yerine direkt olarka delete komutunu da kullabiliriz
        "Delete from $tblProduct where $colId = $id");

    return result;
  }

// veri tabanındaki Seçme ve Listelerler ilgili işlemi burada gerçekleştiriyoruz
  Future<List<Map<String, Object?>>?> getProducts() async {
    Database? db = _db;

    var result = await db?.rawQuery("Select * from $tblProduct");
    return result;
  }
}
