import 'package:flutter/material.dart';
import 'package:flutter_etrade/db/dbHelper.dart';
import 'package:flutter_etrade/models/product.dart';

class ProductList extends StatefulWidget {
  const ProductList({super.key});

  @override
  State<ProductList> createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  DbHelper dbHelper = DbHelper();
  List<Product>? products;
  int count = 5;
  @override
  Widget build(BuildContext context) {
    if (products == null) {
      products = <Product>[];
      getData();
    }
    return Scaffold(
      body: _productListITems(),
    );
  }

  ListView _productListITems() {
    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context, int position) {
        return Card(
          color: Colors.amberAccent,
          elevation: 2,
          child: ListTile(
            leading: const CircleAvatar(
              backgroundColor: Colors.green,
              child: Text("A"),
            ),
            title: Text(products?[position].getName ?? "getname exception"),
            subtitle: Text(
                products?[position].getDescription ?? "getname desscription"),
            onTap: () {},
          ),
        );
      },
    );
  }

  void getData() {
    var db = dbHelper.initializeDb();
    db.then((result) {
      var productsFuture = dbHelper.getProducts();
      productsFuture.then((data) {
        List<Product>? productsData;
        count = data?.length ?? 0;

        for (int i = 0; i < count; i++) {
          productsData?.add(Product.fromObject(data![i]));
        }
        setState(() {
          products = productsData;
          count = count;
        });
      });
    });
  }
}
