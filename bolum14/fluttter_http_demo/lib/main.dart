import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttter_http_demo/apis/api.dart';
import 'package:fluttter_http_demo/models/todo.dart';

void main(List<String> args) => runApp(TodoApp());

class TodoApp extends StatelessWidget {
  const TodoApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: TodoListView(),
    );
  }
}

class TodoListView extends StatefulWidget {
  const TodoListView({super.key});

  @override
  State<TodoListView> createState() => _TodoListViewState();
}

class _TodoListViewState extends State<TodoListView> {
  List<Todo>? todoList;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Todo ListView Page"),
      ),
      body: ListView.builder(
        itemCount: todoList?.length,
        itemBuilder: (context, position) {
          return Card(
            child: ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.amber,
                child: Text("A"),
              ),
              title: Text(todoList?[position].title ?? "title Soysal"),
              subtitle: Text(todoList![position].completed.toString()),
            ),
          );
        },
      ),
    );
  }

  getTodos() {
    Api.getTodos().then((response) {
      setState(() {
        Iterable list = jsonDecode(response.body);
        this.todoList = list.map((model) => Todo.fromJson(model)).toList();
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getTodos();
  }
}
