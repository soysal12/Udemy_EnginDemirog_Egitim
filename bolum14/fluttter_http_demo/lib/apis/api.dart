// ignore_for_file: avoid_print

import 'package:http/http.dart' as http;

class Api {
  static Future getTodos() async {
    var url =
        await http.get(Uri.parse("https://jsonplaceholder.typicode.com/todos"));
    if (url.statusCode == 200) {
      String data = url.body;
      print(data);
    }
    return url;
  }
}
