import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        //  margin: EdgeInsets.all(50),
        alignment: Alignment.center,
        color: Colors.red,
        child: Row(
          textDirection: TextDirection.ltr,
          children: [
            Text(
              "Asus Laptop ",
              textDirection: TextDirection.ltr,
              style: TextStyle(fontSize: 20),
            ),
            Expanded(
              child: Text(
                "  16 GB RAM 512GB SSD  ",
                textDirection: TextDirection.ltr,
                style: TextStyle(fontSize: 50),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
