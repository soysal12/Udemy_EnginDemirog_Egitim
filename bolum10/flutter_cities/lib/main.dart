import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

List<String> cities = ["Şanlıurfa", "Ankara", "van", "İzmir"];
String selectedCity = cities.first;

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const CitySelectorWidget(),
    );
  }
}

class CitySelectorWidget extends StatefulWidget {
  const CitySelectorWidget({super.key});

  @override
  State<CitySelectorWidget> createState() => _CitySelectorWidgetState();
}

class _CitySelectorWidgetState extends State<CitySelectorWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("City Selection App"),
      ),
      body: Column(
        children: <Widget>[
          Center(
            child: DropdownButton(
              elevation: 5,
              value: selectedCity,
              items: cities
                  .map((String value) => DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      ))
                  .toList(),
              onChanged: (value) {
                setState(() {
                  selectedCity = value;
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
