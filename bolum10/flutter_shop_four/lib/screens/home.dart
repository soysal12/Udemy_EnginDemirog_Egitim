import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        //  margin: EdgeInsets.all(50),
        alignment: Alignment.center,
        color: Colors.red,
        child: Column(
          textDirection: TextDirection.ltr,
          children: [
            Row(
              textDirection: TextDirection.ltr,
              children: [
                Text(
                  "Asus Laptop ",
                  textDirection: TextDirection.ltr,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      decoration: TextDecoration.none),
                ),
                Expanded(
                  child: Text(
                    "  16 GB RAM 512GB SSD  ",
                    textDirection: TextDirection.ltr,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 50,
                        decoration: TextDecoration.none),
                  ),
                ),
              ],
            ),
            Row(
              textDirection: TextDirection.ltr,
              children: [
                Text(
                  "HP Laptop ",
                  textDirection: TextDirection.ltr,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      decoration: TextDecoration.none),
                ),
                Expanded(
                  child: Text(
                    "  32 GB RAM 1Tb GB SSD  ",
                    textDirection: TextDirection.ltr,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 50,
                        decoration: TextDecoration.none),
                  ),
                ),
              ],
            ),
            Directionality(
                textDirection: TextDirection.ltr, child: OrderButton()),
          ],
        ),
      ),
    );
  }
}

class OrderButton extends StatelessWidget {
  const OrderButton({super.key});

  @override
  Widget build(BuildContext context) {
    var button = Container(
      margin: EdgeInsets.only(top: 50),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(backgroundColor: Colors.black),
          onPressed: () {
            order(context);
          },
          child: Text("go to check out ")),
    );
    return button;
  }
}

order(BuildContext context) {
  var alert = AlertDialog(
    title: Text("go to check out"),
    content: Text("I will go to check out"),
  );

  showDialog(
    context: context,
    builder: (context) => alert,
  );
}
