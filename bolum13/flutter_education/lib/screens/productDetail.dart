// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter_etrade/db/dbHelper.dart';
import 'package:flutter_etrade/models/product.dart';

// ignore: must_be_immutable
class ProductDetail extends StatefulWidget {
  ProductDetail(this.product, {super.key});
  Product? product;
  @override
  // ignore: no_logic_in_create_state
  State<ProductDetail> createState() => _ProductDetailState(product);
}

DbHelper? dbHelper = DbHelper();

// ignore: constant_identifier_names
enum Choice { Delete, Update } // appbaardaki listenin içindeki menu kıısmı

class _ProductDetailState extends State<ProductDetail> {
  _ProductDetailState(Product? product);
  Product? product;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Product Detail fo ${product?.name}"),
        actions: [
          PopupMenuButton<Choice>(
            onSelected: select,
            itemBuilder: (context) => const <PopupMenuEntry<Choice>>[
              PopupMenuItem<Choice>(
                  value: Choice.Delete, child: Text("Delete Product")),
              PopupMenuItem<Choice>(
                  value: Choice.Update, child: Text("Update Product")),
            ],
          )
        ],
      ),
      body: Center(
        child: Card(
          child: Column(
            children: [
              ListTile(
                leading: Icon(Icons.shop),
                title: Text(product?.name ?? "detail title"),
                subtitle: Text(product?.description ?? "detail subtitle"),
              ),
              Text("${product?.name} =>  price is => ${product?.price} £ "),
              ButtonBarTheme(
                  data: ButtonBarThemeData(),
                  child: ButtonBar(
                    children: [
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.red),
                          onPressed: () {
                            // Carda eklendiğini ekranda mesaj olarak göstermek için yazdım
                            AlertDialog alertDialog = AlertDialog(
                              title: Text("Add to Success"),
                              content: Text("${product?.name} add to card !"),
                            );
                            showDialog(
                              context:
                                  context, // buradakdi antire işlemi parametre göndermeyecğimiz için yazdık
                              //sadece fonk çalıştıracağı için
                              builder: (_) => alertDialog,
                            );
                          },
                          child: Text("add to card ")),
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }

  void select(Choice choice) async {
    int? result;

    switch (choice) {
      case Choice.Delete:
        Navigator.pop(context);
        result = await dbHelper?.delete(product?.id ?? 0);
        if (result != 0) {
          AlertDialog alertDialog = AlertDialog(
            title: const Text("Success"),
            content: Text("Deleted Product ${product?.name}"),
          );
          showDialog(
            context:
                context, // buradakdi antire işlemi parametre göndermeyecğimiz için yazdık
            //sadece fonk çalıştıracağı için
            builder: (_) => alertDialog,
          );
        }
        break;
      default:
    }
  }
}
