// ignore_for_file: unnecessary_null_comparison

import 'package:flutter/material.dart';
import 'package:flutter_etrade/db/dbHelper.dart';
import 'package:flutter_etrade/models/product.dart';
import 'package:flutter_etrade/screens/productAdd.dart';
import 'package:flutter_etrade/screens/productDetail.dart';

class ProductList extends StatefulWidget {
  const ProductList({super.key});

  @override
  State<ProductList> createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  DbHelper dbHelper = DbHelper();
  List<Product> products = <Product>[];
  int count = 0;
  @override
  Widget build(BuildContext context) {
    if (products == null) {
      products = <Product>[];
      getData();
    }
    return Scaffold(
      body: _productListITems(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          gotoProductAdd();
        },
        child: Icon(Icons.add),
      ),
    );
  }

  ListView _productListITems() {
    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context, int position) {
        return Card(
          shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.red),
              borderRadius: BorderRadius.circular(20)),
          color: Colors.amberAccent,
          elevation: 2.0,
          child: ListTile(
            leading: const CircleAvatar(
              backgroundColor: Colors.green,
              child: Text("A"),
            ),
            title: Text(products[position].name ?? "MUZO"),
            subtitle: Text(products[position].description ?? "muzaffer"),
            onTap: () {
              gotoDetail(products[position]);
            },
          ),
        );
      },
    );
  }

  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var productsFuture = dbHelper.getProducts();
      productsFuture.then((data) {
        List<Product> productsData = <Product>[];
        count = data?.length ?? 0;

        for (int i = 0; i < count; i++) {
          productsData.add(Product.fromObject(data?[i]));
        }
        setState(() {
          products = productsData;
          count = count;
        });
      });
    });
  }

  void gotoDetail(Product product) async {
    bool result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ProductDetail(product),
        ));
    if (result != null) {
      if (result) {
        getData();
      }
    }
  }

  void gotoProductAdd() async {
    // Ürün ekleme kısmının set etme yeri
    var result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ProductAdd(),
        ));
    if (result != null) {
      if (result) {
        getData(); // datayı güncellemek için yaptık yeni ürün ekledğimiz için
      }
    }
  }
}
