// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_etrade/db/dbHelper.dart';
import 'package:flutter_etrade/models/product.dart';

class ProductAdd extends StatefulWidget {
  const ProductAdd({super.key});

  @override
  State<ProductAdd> createState() => _ProductAddState();
}

class _ProductAddState extends State<ProductAdd> {
  TextEditingController textNameController = TextEditingController();
  TextEditingController textDescriptionController = TextEditingController();
  TextEditingController textPriceController = TextEditingController();

  DbHelper dbHelper = DbHelper();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(" Add a new Product"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
        child: Column(
          children: [
            TextField(
              controller: textNameController,
              decoration: const InputDecoration(labelText: "Name"),
            ),
            TextField(
              controller: textDescriptionController,
              decoration: const InputDecoration(labelText: "Description"),
            ),
            TextField(
              controller: textPriceController,
              decoration: const InputDecoration(labelText: "price"),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  save();
                },
                child: const Text("Save"))
          ],
        ),
      ),
    );
  }

  void save() async {
    int? result = await dbHelper.insert(Product(
        textNameController.text,
        textDescriptionController.text,
        double.tryParse(textPriceController.text)));

    if (result != 0) {
      Navigator.pop(context, true);
    }
  }
}
