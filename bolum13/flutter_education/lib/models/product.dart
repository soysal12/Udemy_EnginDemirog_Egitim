// hocadaki id bende getId dir diğerleride buna benzerdir

class Product {
  int? _id;
  String? _name;
  String? _description;
  double? _price;

  Product(this._name, this._description, this._price);
  Product.withId(this._id, this._name, this._description, this._price);

  int? get id => _id;
  String? get name => _name;
  String? get description => _description;
  double? get price => _price;

  set nameSet(String newName) {
    if (newName.length >= 2) {
      _name = newName;
    }
  }

  set setDescription(String newDescription) {
    if (newDescription.length >= 2) {
      _description = newDescription;
    }
  }

  set setPrice(double newPrice) {
    if (newPrice >= 10) {
      _price = newPrice;
    }
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};

    map["name"] = _name;
    map["description"] = _description;
    map["price"] = _price;
    if (_id != null) {
      map["id"] = _id;
    }

    return map;
  }

  Product.fromObject(dynamic o) {
    //gelen verileri kendi tanımladığımız dataya göre yerleştirdik
    _id = o["Id"];
    _name = o["Name"];
    _description = o["Description"];
    _price = double.tryParse(o["Price"].toString());
    // sqflite da database de double değer tutamadığımız için
    //price buradadn double ye çevirdik
  }
}
