import 'package:flutter/material.dart';
import 'package:flutter_etrade/models/product.dart';
import 'package:flutter_etrade/screens/productDetail.dart';
import 'package:flutter_etrade/screens/productList.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  Product? product;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // uzun yorum kısmı deneme amaçlı yazılmıştır

    /*List<Product>? products; // ürünlerimi products adında listeye attım
    DbHelper dbHelper = new DbHelper();
    // ürünlerimi çektiğim fonk devreye koydum

    dbHelper.initializeDb().then((result) => dbHelper.getProducts().then(
        (value) => products =
            value?.cast<Product>())); // buradad ürünlerimi listeledim

// veri tabanıma yeni ürün eklemesi yapıyorum
    Product product = new Product("Mouse", "A4 Tech wireless Mouse", 1);
    dbHelper.insert(product); */

    //  late List<Product> products;
    // DbHelper dbHelper = DbHelper();

    // dbHelper.initializeDb().then((result) => dbHelper
    //     .getProducts()
    //     .then((value) => products = value?.cast<Product>()));

    // Product product = Product("Mouse", "A4 Tech wireless Mouse", 1);
    // dbHelper.insert(product);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Welcome To Soysal DataBase"),
      ),
      body: const ProductList(),
    );
  }
}
